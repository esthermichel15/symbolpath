# Created By: Karl Wiegand (wiegand@ccs.neu.edu)0
# Date Created: Sat Jan 28 10:35:00 EST 2012
# Description: Sem-gram statistics from a SSTATS file
# $Id$

from __future__ import division
import operator

class SemStats():
    """
    Stores the sem-gram statistics from a SSTATS file
    and makes them accessible from a single object.

    Members:
        gramCounts: gram -> count
            (str, str, ...) -> int

    ATTN: Please keep unit tests relevant!
    """

    def __init__(this, semStatsFileName):
        """
        Read the statistics from the specified SSTATS file.

        Input:
            semStatsFileName -- (string) SSTATS file name

        Raises an Exception if anything went wrong.
        """
        this.semStatsFileName = semStatsFileName
        this.gramCounts = {}

        # Read the SSTATS file
        try:
            with open(semStatsFileName) as inFile:
                lines = inFile.read().lower().strip().split("\n")
        except Exception:
            raise Exception("Unable to read statistics file!")

        # Parse the statistics data as CSV:
        #   - tuple of strings (the sem-gram)
        #   - integer >= 1 (count of that sem-gram)
        for line in lines:

            # Skip empty lines
            if len(line.strip()) <= 0:
                continue

            # Skip comment lines
            if line.strip().startswith("#"):
                continue

            # Assign the data elements
            elements = [item.lower().strip() for item in line.strip().split(",")]
            gram = tuple(sorted(elements[:-1]))
            count = int(elements[-1])

            # Store the data elements
            this.gramCounts[gram] = count

    def getCount(this, gram):
        """
        Given a sem-gram, return the smoothed count.

        Input:
            gram -- (tuple(str)) Semantic gram as a tuple(strings)

        Returns an integer count >= 1.
        """
        if gram in this.gramCounts:
            return this.gramCounts[gram] + 1
        else:
            return 1

    def getGramCounts(this, gramList):
        """
        Given a list of sem-grams, return pairs of the gram and
        smoothed count, in decreasing order of likelihood (greatest
        to least).

        Input:
            gramList -- listof(tuple(str)) List of semantic grams

        Returns a list of pairs (gram, count).
        """
        retList = [(gram, this.getCount(gram)) for gram in gramList]
        retList = sorted(retList, key = operator.itemgetter(1), reverse = True)
        return retList

    def getWordCounts(this, wordList, partGram):
        """
        Given a list of words and a partial sem-gram, return pairs
        of the words a smoothed count, in decreasing order of likelihood
        (greatest to least).

        Input:
            wordList -- listof(str) List of words
            partGram -- Semantic gram as a tuple(strings)

        Returns a list of pairs (word, count).
        """
        retList = [(word, this.getCount(tuple(sorted(list(partGram) + [word]))))
            for word in wordList]
        retList = sorted(retList, key = operator.itemgetter(1), reverse = True)
        return retList

    def countsToProbs(this, itemList):
        """
        Given a listof(item, count), replace the counts with
        marginalized probabilities, in decreasing order of likelihood
        (greatest to least).
        
        Input:
            itemList -- listof((any, int)) List of tuples(item, count)

        Returns a list of pairs (item, prob), where all probs sum to 1.
        """
        total = sum([item[1] for item in itemList])
        retList = [(item[0], item[1]/total) for item in itemList]
        retList = sorted(retList, key = operator.itemgetter(1), reverse = True)
        return retList
