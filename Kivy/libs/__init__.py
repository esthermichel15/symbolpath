# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Feb 14 13:44:57 EST 2013
# Description: Module initialization of "libs"

# Optional description of all modules in this package;
# used for "from libs import *" command

__all__ = [
        "semstats", # Sem-gram statistics from a STATS file
        "tts", # Support for Text-to-Speech (TTS) engines
        "vocab", # Vocabulary configuration from a VOCAB file
        ]
