# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Sat Jan 28 10:35:00 EST 2012
# Description: Vocabulary configuration from a VOCAB file
# $Id: ini.py,v 0ee7bb76b481 2012/02/02 18:44:40 wiegand $

class Vocab():
    """
    Stores the vocabulary from a VOCAB file and
    makes them accessible from a single object.

    Members:
        roleWords: role -> [word, word, word, ...]
            str -> listof(str)
        fileWords: file_name -> word
            str -> str
        wordFiles: word -> file_name
            str -> str
        wordRoles: word -> [role, role, role, ...]
            str -> listof(str)

    ATTN: Please keep unit tests relevant!
    """

    def __init__(this, vocabFileName):
        """
        Read the vocabulary from the given VOCAB file.

        Input:
            vocabFileName -- (string) VOCAB file name

        Raises an Exception if anything went wrong.
        """
        this.vocabFileName = vocabFileName
        this.roleWords = {}
        this.fileWords = {}
        this.wordFiles = {}
        this.wordRoles = {}

        # Read the VOCAB file
        try:
            with open(vocabFileName) as inFile:
                lines = inFile.read().split("\n")
        except Exception:
            raise Exception("Unable to read vocabulary file!")

        # Parse the vocabulary data as CSV:
        #   - name/word/label
        #   - image file name
        #   - roles
        for line in lines:

            # Skip empty lines
            if len(line.strip()) <= 0:
                continue

            # Skip comment lines
            if line.strip().startswith("#"):
                continue

            # Assign the data elements
            elements = [item.lower().strip() for item in line.strip().split(",")]
            word = elements[0]
            wordFile = elements[1]
            if len(wordFile) <= 0:
                wordFile = word
            wordRoles = elements[2:]

            # Organize and store the data elements
            for role in wordRoles:
                if role in this.roleWords:
                    this.roleWords[role].append(word)
                    this.roleWords[role].sort()
                else:
                    this.roleWords[role] = [word]
            this.fileWords[wordFile] = word
            this.wordFiles[word] = wordFile
            this.wordRoles[word] = sorted(wordRoles)
