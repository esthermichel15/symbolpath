# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Wed Feb 13 12:44:22 EST 2013
# Description: Code for SymbolPath GUI

# Operator adjustments
from __future__ import division

# Standard libraries
import math

# Kivy framework
import kivy
kivy.require('1.5.1')
from kivy.graphics import Color, GraphicException, Point, Rectangle
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.widget import Widget

class IconScreen(Screen):
    """
    Icon selection grid.
    """

    def __init__(this, **kwargs):
        """
        Initialize the Screen.

        Input:
            postTouchUpFunc -- (callback) function pointer
                to method that should be called after an
                on_touch_up() event

        Raises an Exception if anything went wrong.
        """
        # Call our parent's constructor
        super(IconScreen, this).__init__()
        
        this.images = {}

        # Assign our arguments
        this.postTouchUpFunc = lambda: None
        if "postTouchUpFunc" in kwargs:
            this.postTouchUpFunc = kwargs["postTouchUpFunc"]

    def __interpolatePoints(this, startX, startY, endX, endY, stepSize = 5):
        """
        Interpolate intermediate points between the given two
        endpoints at the specified level of granularity.
        
        Input:
            startX -- (float) X-coordinate of the starting endpoint
            startY -- (float) Y-coordinate of the starting endpoint
            endX -- (float) X-coordinate of the other endpoint
            endY -- (float Y-coordinate of the other endpoint
            stepSize -- (int) Size of steps to interpolate

        Returns a list of coordinate pairs as tuples; raises
        an Exception if anything went wrong.
        """
        points = []

        # Calculate interpolation steps
        deltaX = endX - startX
        deltaY = endY - startY
        distance = math.sqrt(deltaX ** 2 + deltaY ** 2)
        numSteps = int(distance / stepSize)

        # Apply steps to create point coordinates
        for stepNum in xrange(1, numSteps):
            stepRatio = stepNum / numSteps
            x = startX + deltaX * stepRatio
            y = startY + deltaY * stepRatio
            points.append((x, y))

        return points

    def setGridIcons(this, names, colors):
        iconW = 1.0 / math.ceil(len(names) / 10)
        iconH = 1.0 / 10
        posX = 0.0
        posY = 1.0 - iconH
        for name in names:
            icon = BoxLayout(orientation = "vertical", padding = 5)
            icon.size_hint = (iconW, iconH)
            icon.pos_hint = {"x" : posX, "y" : posY}
            icon.name = name
            icon.color = colors.pop(0)
            icon.bind(pos = this.resized)
            this.add_widget(icon)

            posY -= iconH
            if posY < 0.0:
                posY = 1.0 - iconH
                posX += iconW

    def resized(this, instance, value):
        instance.canvas.clear()
        instance.canvas.add(Color(*instance.color))
        instance.canvas.add(Rectangle(pos = (instance.x + 1.5,
            instance.y + 1.5), size = (instance.width - 3,
                instance.height - 3)))
        instance.clear_widgets()
        lbl = Label(text = instance.name, color = [0, 0, 0, 1], halign = "center",
                valign = "top")
        img = this.images[instance.name]
        img.size_hint = (1, 0.8)
        lbl.size_hint = (1, 0.2)
        lbl.font_size = lbl.font_size * 1.1
        lbl.texture_update()
        instance.add_widget(img)
        instance.add_widget(lbl)

    def on_touch_down(this, touch):
        """
        Kivy's event handler for when a touch is created
        (multi-touch supported); display a colored touch
        and prepare for tracking its future position.

        Input:
            touch -- (Kivy MotionEvent) Touch event information

        Raises an Exception if anything goes wrong.
        """
        # Assign a unique group ID based on the touch
        touch.ud["group"] = str(touch.uid)

        # Store the touch information for drawing
        with this.canvas:

            # Generate and store the color
            touch.ud["color"] = Color(
                    *kivy.utils.get_random_color(),
                    mode = "HSV",
                    group = touch.ud["group"]
                    )

            # Draw and track the points
            touch.ud["lines"] = [Point(
                    points = (touch.x, touch.y),
                    pointsize = 10,
                    source = "images/touch.png",
                    group = touch.ud["group"]
                    )]

    def on_touch_move(this, touch):
        """
        Kivy's event handler for when a touch moves
        (multi-touch supported); draw a smooth line for
        each particular touch.

        Input:
            touch -- (Kivy MotionEvent) Touch event information

        Raises an Exception if anything goes wrong.
        """
        # Load this touch's points
        points = touch.ud["lines"][-1].points

        # Grab the coordinates of the last point
        lastX, lastY = points[-2], points[-1]

        # Interpolate points to the new coordinates
        points = this.__interpolatePoints(lastX, lastY, touch.x, touch.y)

        # Draw the points
        for point in points:
            while True:
                try:
                    touch.ud["lines"][-1].add_point(*point)
                except GraphicException:

                    # Add a new Point if we hit Kivy's
                    # coordinate limit of 2^15 - 2 
                    with this.canvas:
                        touch.ud["lines"].append(Point(
                            points = (touch.x, touch.y),
                            pointsize = 10,
                            source = "images/touch.png",
                            group = touch.ud["group"]
                            ))
                    continue

                break

    def loadImage(this, fileName, name):
        this.images[name] = Image(source = fileName)

    def on_touch_up(this, touch):
        """
        Kivy's event handler for when a touch disappears
        (multi-touch supported); remove associated points
        from the drawing queue.
        
        Input:
            touch -- (Kivy MotionEvent) Touch event information

        Raises an Exception if anything goes wrong.
        """
        # Remove the line, if necessary
        if "group" in touch.ud:
            this.canvas.remove_group(touch.ud["group"])

        # Call our given function
        this.postTouchUpFunc()

class UttScreen(Screen):
    """
    Icon selection grid.
    """
    pass
