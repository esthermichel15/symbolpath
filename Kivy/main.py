#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Tue Feb 12 15:00:33 EST 2013
# Description: Main program file for SymbolPath

# Standard libraries
import os

# Kivy framework
import kivy
kivy.require('1.5.1')
from kivy.app import App
from kivy.config import ConfigParser
from kivy.lang import Builder
from kivy.logger import Logger
from kivy.uix.screenmanager import ScreenManager
from kivy.uix.settings import Settings

# App-specific imports
from libs import *
from GUI import *

class SymbolPath(App):
    """
    Main class for an instance of SymbolPath.
    """

    def build_config(this, config):
        """
        Initialization of a Kivy configuration.
        """
        config.setdefaults("GUI",
                {
                    "MaxUtterances" : 10,
                    "NumRows" : 10,
                    "PathWidth" : 10
                })

    def build_settings(this, settings):
        settings.add_json_panel("SymbolPath", this.config, "symbolpath.json")

    def build(this):
        """
        Initialization of a Kivy App.

        Returns the root Widget.
        """
        Logger.info("SP: Loading TTS")
        this.tts = tts.TTS()

        Logger.info("SP: Parsing vocabulary file")
        vocabFileName = "Crowd.vocab"
        this.vocab = vocab.Vocab(vocabFileName)

        Logger.info("SP: Parsing statistics file")
        statsFileName = "Crowd.stats"
        this.stats = semstats.SemStats(statsFileName)

        Logger.info("SP: Initializing screens")
        this.gui = ScreenManager(duration = 0.001)
        this.iconScreen = IconScreen(name = "icons",
                postTouchUpFunc = this.speak)
        this.gui.add_widget(this.iconScreen)
        this.gui.add_widget(UttScreen(name = "utts"))

        Logger.info("SP: Loading vocabulary images")
        desiredFiles = {}
        for fileName in this.vocab.fileWords.keys():
            desiredFiles[fileName] = True
        for path, dirs, files in os.walk("icons", True):
            for file in files:
                fullPath = os.path.abspath(os.path.join(path, file))
                prefix = os.path.splitext(file)[0].strip()
                if prefix in desiredFiles:
                    try:
                        this.iconScreen.loadImage(fullPath, this.vocab.fileWords[prefix])
                    except:
                        Logger.error("Unable to load image -- " + prefix)
                    else:
                        del desiredFiles[prefix]

        Logger.info("Checking for missing vocabulary icons")
        for missingIcon in sorted(desiredFiles.keys()):
            Logger.critical("Missing image file for -- " + missingIcon)
        if len(desiredFiles.keys()) > 0:
            raise Exception("Unable to load all required image files")

        Logger.info("Arranging icons in the vocabulary")
        orderedIcons = (this.vocab.roleWords["actor"] +
                this.vocab.roleWords["pred"] +
                this.vocab.roleWords["object"] +
                this.vocab.roleWords["mod"])
        iconColors = []
        for w in this.vocab.roleWords["actor"]:
            iconColors.append((0.66, 0.9, .22, 0.4))
        for w in this.vocab.roleWords["pred"]:
            iconColors.append((0.15, 0.76, 0.93, 0.3))
        for w in this.vocab.roleWords["object"]:
            iconColors.append((0.98, 0.67, 0.27, 0.4))
        for w in this.vocab.roleWords["mod"]:
            iconColors.append((0.59, 0.32, 0.93, 0.3))
        this.iconScreen.setGridIcons(orderedIcons, iconColors)

        return this.gui

    def on_pause(this):
        """
        Can this App pause?

        Returns True.
        """
        return True

    def speak(this):
        this.tts.speak("I like to play chess with my brother.")

# Create and run an instance of SymbolPath
# when executed directly
if __name__ == '__main__':
    SymbolPath().run()
