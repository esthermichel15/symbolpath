Software Dependencies:
    - Python 2.7 -- http://www.python.org/download/
        - On OS X, please upgrade from System Python to Standard Python.
    - Kivy 1.5.1 -- http://kivy.org/#download

To Run the Program:
    - Install Standard Python
    - Install Kivy
    - Open a terminal and run [python main.py]
        - On Windows, you may need to run [kivy main.py]
