#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: 10:54 AM Friday, July 15, 2011
# Description: Main program for SymbolPath
# $Id: SymbolPath.py,v 7df9332ab64e 2012/06/29 14:42:08 wiegand $

import os
import mutex
import pygame
import sys
import time

# Custom modules
from neu import *

class SymbolPath:
    """
    Main class for an instance of SymbolPath.
    """
    def __init__(this,
            logFileName = "SymbolPath.log",
            iniFileName = "SymbolPath.ini"):
        """
        Initialize internal data structures, such as
        lists of predicates and arguments.

        Input:
            logFileName -- (string) file name for error log
            iniFileName -- (string) INI file name
        """
        # Start or continue the log file
        try:
            this.log = log.LogFile(logFileName)
        except Exception, e:
            print str(e)
            print "Error: Unable to open log file -- " + \
                logFileName
            raise

        # Read the INI file
        try:
            iniFile = ini.IniFile(iniFileName)

            # Load the predicate->image map
            this.log.log("Progress: Loading predicate image files...")
            this.predicateMap = {}
            predicates = iniFile.getPredicates()
            predicateDir = iniFile.getPredicateDir()
            for path, dirs, files in os.walk(predicateDir, True):
                for file in files:
                    fullPath = os.path.abspath(os.path.join(path, file))
                    prefix = os.path.splitext(file)[0].strip().lower()
                    if prefix in predicates:
                        predicates.remove(prefix)
                        img = pygame.image.load(fullPath)
                        this.predicateMap[prefix] = img

            # Load the argument->image map
            this.log.log("Progress: Loading argument image files...")
            this.argumentMap = {}
            arguments = iniFile.getArguments()
            argumentDir = iniFile.getArgumentDir()
            for path, dirs, files in os.walk(argumentDir, True):
                for file in files:
                    fullPath = os.path.abspath(os.path.join(path, file))
                    prefix = os.path.splitext(file)[0].strip().lower()
                    if prefix in arguments:
                        arguments.remove(prefix)
                        img = pygame.image.load(fullPath)
                        this.argumentMap[prefix] = img

            # Load the command->image map
            this.log.log("Progress: Loading system command image files...")
            this.commandMap = {}
            commands = iniFile.getCommands()
            commandDir = iniFile.getCommandDir()
            for path, dirs, files in os.walk(commandDir, True):
                for file in files:
                    fullPath = os.path.abspath(os.path.join(path, file))
                    prefix = os.path.splitext(file)[0].strip().lower()
                    if prefix in commands:
                        commands.remove(prefix)
                        img = pygame.image.load(fullPath)
                        this.commandMap[prefix] = img

            # Are we missing any predicate images?
            if len(predicates) > 0:
                this.log.log("Warning: Predicate images missing -- " + \
                    ", ".join(predicates))

            # Are we missing any argument images?
            if len(arguments) > 0:
                this.log.log("Warning: Argument images missing -- " + \
                    ", ".join(arguments))

            # Are we missing any system command images?
            if len(commands) > 0:
                this.log.log("Warning: Command images missing -- " + \
                    ", ".join(commands))

        except Exception, e:
            this.log.log(str(e))
            this.log.log("Error: Unable to load INI file -- " + \
                iniFileName)
            raise

        # Load the predicate->listof(categories) map
        this.log.log("Progress: Loading predicate categories...")
        this.predicateRoles = iniFile.getPredicateRoles()

        # Load the argument maps
        this.log.log("Progress: Loading argument maps...")
        this.actorMap = {}
        this.modMap = {}
        this.objMap = {}
        argumentRoles = iniFile.getArgumentRoles()
        for argRoles in argumentRoles:
            arg = argRoles[0]
            if arg not in this.argumentMap:
                continue
            categories = argRoles[1]
            if len(categories) <= 0:
                this.objMap[arg] = this.argumentMap[arg]
            else:
                for category in categories:
                    if category.endswith("Mod") or category == "count":
                        this.modMap[arg] = this.argumentMap[arg]
                    elif category == "actor" or category == "part":
                        this.actorMap[arg] = this.argumentMap[arg]
                    elif category == "object":
                        this.objMap[arg] = this.argumentMap[arg]

        # Store the TTS command
        this.TTSCommand = iniFile.getTTSCommand()

        # Init the sequence counter and message text
        this.textMap = {}
        this.text = ""
        this.words = {}
        this.wordVotes = {}
        this.firstPred = ""
        this.firstActor = ""
        this.lastObj = ""
        this.lastMod = ""

        # Init the system state
        this.sliding = False

    def getMostPopular(this, role, words, wordVotes, first = None, last = None):
        """
        Return the most popular word based on votes.

        Input:
            role -- string -- One of the roles in the "words" dict.
            words -- dict (string/word -> string/role) -- Available
                words to search.
            wordVotes -- dict (string/word -> int/votes) -- Ballots
                cast for each available word.
            first -- string -- First word touched.
            last -- string -- Last word touched.

        Returns:
            string -- The most popular word with the specified role.
        """
        retWord = ""
        numVotes = 0
        for word, votes in wordVotes.iteritems():
            if word == first:
                votes = votes * 3.5 # First word is worth more
            if word == last:
                votes = votes * 3 # Last word is worth more
            if words[word] == role and votes > numVotes:
                retWord = word
                numVotes = votes
        return retWord

    def run(this):
        """
        Display and run the interactive GUI.

        Returns an integer: 0 if it completes without
        errors, otherwise a non-zero error code.
        """
        # Load the SDL drivers
        pygame.init()

        # Configure and show the GUI
        try:
            this.GUI = gui.GUI(this.log, this.actorMap, this.predicateMap, this.modMap, this.objMap)
        except Exception, e:
            this.log.log("Exception: " + str(e))
            this.log.log("Error: Failed to initialize GUI!")
            return 1
        this.GUI.reset()

        # Initialize the message construction engine
        try:
            this.msg = msg.Message(this.log)
        except Exception, e:
            this.log.log("Exception: " + str(e))
            this.log.log("Error: Failed to initialize message constructor!")

        # Initialize the Text-to-Speech engine
        try:
            this.TTS = tts.TTS(this.log, this.TTSCommand)
        except Exception, e:
            this.log.log("Exception: " + str(e))
            this.log.log("Error: Failed to initialize TTS engine!")
        
        # Wait for events
        while True:
            event = pygame.event.wait()
            
            # Any key press
            if event.type == pygame.KEYDOWN:
                this.log.log("User: Keydown -- " + \
                    pygame.key.name(event.key))

                # Escape button quits
                if event.key == pygame.K_ESCAPE:
                    break

            # Mouse button pressed
            elif event.type == pygame.MOUSEBUTTONDOWN:
                this.log.log("User: Mousedown -- " + str(event.button))
                this.sliding = True
            
            # Mouse button released
            elif event.type == pygame.MOUSEBUTTONUP:
                this.log.log("User: Mouseup -- " + str(event.button))

                # Construct and show the message
                this.textMap["pred"] = this.getMostPopular("pred", this.words, this.wordVotes, this.firstPred)
                this.textMap["actor"] = this.getMostPopular("actor", this.words, this.wordVotes, this.firstActor)
                this.textMap["object"] = this.getMostPopular("obj", this.words, this.wordVotes, None, this.lastObj)
                if this.lastMod != "":
                    this.textMap["objectMod"] = this.getMostPopular("mod", this.words, this.wordVotes, None, this.lastMod)
                this.text = this.msg.make(this.textMap)
                if len(this.text) > 1:
                    this.GUI.setText(this.text)
                    this.TTS.speak(this.log, this.text)
                else:
                    this.GUI.setText("")

                # Take a screenshot
                pygame.image.save(this.GUI.screen, "screenshot.bmp")
                this.sliding = False
                this.GUI.reset()

                # Log the data for analysis later
                this.log.log("System: Words -- " + str(this.words))
                this.log.log("System: Votes -- " + str(this.wordVotes))
                this.log.log("System: Text -- " + str(this.text))

                # Reset everything
                this.words = {}
                this.wordVotes = {}
                this.textMap = {}
                this.text = ""
                this.firstPred = ""
                this.firstActor = ""
                this.lastObj = ""
                this.lastMod = ""
            
            # Mouse moving
            elif event.type == pygame.MOUSEMOTION:
                if this.sliding:
                    this.log.log("User: Moving cursor -- " + str(event.pos))
                    role, word = this.GUI.slide(event.pos)
                    if role != "" and word != "":
                        this.words[word] = role
                        if word in this.wordVotes:
                            this.wordVotes[word] = this.wordVotes[word] + 1
                        else:
                            this.wordVotes[word] = 1
                    if role == "pred" and this.firstPred == "":
                        this.firstPred = word
                    if role == "actor" and this.firstActor == "":
                        this.firstActor = word
                    if role == "obj":
                        this.lastObj = word
                    if role == "mod":
                        this.lastMod = word
        
        # Clean up and return no errors
        this.log.log("Progress: Exiting application...")
        return 0

# Create and run an instance of SymbolPath if
# executed directly
if __name__ == "__main__":
    try:
        sc = SymbolPath()
    except:
        print "Error: Aborting SymbolPath!"
        sys.exit(1)
    else:
        sys.exit(sc.run())
