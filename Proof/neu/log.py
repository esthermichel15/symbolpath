#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Jan 14 11:38:46 EST 2010
# Description: Error logging mechanism
# $Id: log.py,v 60ff49b304c1 2012/05/15 20:04:53 wiegand $

import datetime
import os

class LogFile:
    """
    Error logging mechanism that prints information
    to the screen and stores it in a file.
    """
    def __init__(this, logFileName):
        """
        Create the desired log file if it doesn't
        already exist.

        Input:
            logFileName -- (string) file name for error log

        Raises an Exception if anything went wrong.
        """
        # Remember the file name
        this.logFileName = logFileName

        # Try to open and append to the file
        file = open(this.logFileName, "a")
        file.close()

    def log(this, msg):
        """
        Show the message and write it to the log
        file, prepended with a timestamp.
        """
        print msg
        timestamp = "[" + str(datetime.datetime.now()) + "]"
        file = open(this.logFileName, "a")
        file.write(timestamp + " " + msg + "\n")
        file.close()
