#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Jan 14 11:38:46 EST 2010
# Description: Initialization configuration from INI files
# $Id: ini.py,v 60ff49b304c1 2012/05/15 20:04:53 wiegand $

import ConfigParser

class IniFile:
    """
    Stores the relevant settings from an INI file.
    """
    def __init__(this, iniFileName):
        """
        Read the settings from the given INI file.

        Input:
            iniFileName -- (string) INI file name

        Raises an Exception if anything went wrong.
        """
        this.settings = ConfigParser.SafeConfigParser()
        this.settings.read(iniFileName)

    def getPredicateDir(this):
        """
        Returns the (string) directory containing image
        files of predicates.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("Predicates", "ImageDir")

    def getPredicateFile(this):
        """
        Returns the (string) file name of the index
        file containing a list of predicates.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("Predicates", "IndexFile")

    def getPredicates(this):
        """
        Returns the listof(string) predicates.

        Raises an Exception if anything went wrong.
        """
        fileName = this.getPredicateFile()
        file = open(fileName, "r")
        lines = file.read().split("\n")
        file.close()

        predicates = []
        for line in lines:
            if len(line.strip()) > 0:
                verb = line.split()[0].lower()
                predicates.append(verb)
        return predicates

    def getPredicateRoles(this):
        """
        Returns the string->listof(string) predicates
        and semantic categories.

        Raises an Exception if anything went wrong.
        """
        fileName = this.getPredicateFile()
        file = open(fileName, "r")
        lines = file.read().split("\n")
        file.close()

        predicates = {}
        for line in lines:
            if len(line.strip()) > 0:
                verb = line.split()[0].lower()
                categories = []
                if len(line.split()) > 1:
                    categories = line.split()[1:]
                predicates[verb] = categories
        return predicates

    def getArgumentDir(this):
        """
        Returns the (string) directory containing image
        files of arguments.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("Arguments", "ImageDir")

    def getArgumentFile(this):
        """
        Returns the (string) file name of the index
        file containing a list of arguments and valid
        semantic roles.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("Arguments", "IndexFile")

    def getArguments(this):
        """
        Returns the listof(string) arguments.

        Raises an Exception if anything went wrong.
        """
        fileName = this.getArgumentFile()
        file = open(fileName, "r")
        lines = file.read().split("\n")
        file.close()

        arguments = []
        for line in lines:
            if len(line.strip()) > 0:
                arg = line.split()[0].lower()
                arguments.append(arg)
        return arguments

    def getArgumentRoles(this):
        """
        Returns the listof((string), listof(string)) arguments
        and semantic categories.

        Raises an Exception if anything went wrong.
        """
        fileName = this.getArgumentFile()
        file = open(fileName, "r")
        lines = file.read().split("\n")
        file.close()

        arguments = []
        for line in lines:
            if len(line.strip()) > 0:
                arg = line.split()[0].lower()
                categories = []
                if len(line.split()) > 1:
                    categories = line.split()[1:]
                arguments.append((arg, categories))
        return arguments
        
    def getArgumentRolesPOS(this):
        """
        Returns the listof((string), listof(string)) arguments
        and semantic categories based on each word's part-of-speech (POS).
        
        Raises an Exception if anything went wrong.
        """
        fileName = this.getArgumentFile()
        file = open(fileName, "r")
        lines = file.read().split("\n")
        file.close()

        arguments = []
        for line in lines:
            if len(line.strip()) > 0:
                arg = line.split()[0].lower()
                categories = []
                pos = this.suggest.getPOS(arg)
                if pos == "a":
                    categories = ["actorMod", "partMod", "objectMod"]
                elif pos == "n":
                    categories = ["actor", "part", "object"]
                else:
                    categories = []
                arguments.append((arg, categories))
        return arguments

    def getCommandDir(this):
        """
        Returns the (string) directory containing image
        files of system commands.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("Commands", "ImageDir")

    def getCommandFile(this):
        """
        Returns the (string) file name of the index
        file containing a list of system commands.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("Commands", "IndexFile")

    def getCommands(this):
        """
        Returns the listof(string) system commands.

        Raises an Exception if anything went wrong.
        """
        fileName = this.getCommandFile()
        file = open(fileName, "r")
        lines = file.read().split("\n")
        file.close()

        commands = []
        for line in lines:
            if len(line.strip()) > 0:
                commands.append(line.lower())
        return commands

    def getTTSCommand(this):
        """
        Returns the string system command that will be
        used to call the Text-to-Speech (TTS) engine; this
        string will contain the substring TEXT which
        should be replaced with the text to be spoken.

        Raises an Exception if anything went wrong.
        """
        return this.settings.get("TTS", "Command")
