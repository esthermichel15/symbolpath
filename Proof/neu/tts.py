#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Jan 21 11:44:25 EST 2010
# Description: Routines for accessing the
#   Text-to-Speech (TTS) engine
# $Id: tts.py,v 60ff49b304c1 2012/05/15 20:04:53 wiegand $

import subprocess

# Custom modules
import log

class TTS:
    """
    Object that handles Text-to-Speech (TTS) tasks.
    """
    def __init__(this, log, cmd):
        """
        Sets up the initial TTS system.
        
        Input:
            log -- (neu.log.log) error logging mechanism
            cmd -- (string) TTS system command that contains the
                substring "TEXT" which will be replaced with
                the text to be spoken

        Raises an Exception if anything went wrong.
        """
        if cmd.find("TEXT") < 0:
            log.log("Error: No \"TEXT\" substring in TTS command!")
            raise Exception()
        else:
            this.precmd = cmd.partition("TEXT")[0]
            this.postcmd = cmd.partition("TEXT")[2]

    def speak(this, log, text):
        """
        Calls the precmd and postcmd from the system prompt,
        with the given text in between, in order to access
        the TTS engine and "speak" the text.

        Input:
            log -- (neu.log.log) error logging mechanism
            text -- (string) the text that will be spoken
        """
        cmd = this.precmd + text + this.postcmd
        try:
            retCode = subprocess.call(cmd, shell = True)
            if retCode < 0:
                log.log("Warning: TTS process returned -- " + str(retCode))
        except OSError, e:
            log.log("Error: TTS process failed -- " + str(e))
