#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Jan 21 11:44:25 EST 2010
# Description: Routines for creating and manipulating
#   the GUI via the SDL
# $Id: gui.py,v 7df9332ab64e 2012/06/29 14:42:08 wiegand $

from __future__ import division
import math
import pygame

# Custom modules
import log

class GUI:
    """
    User interface object that interacts with the display
    via the SDL.

    Input:
        log -- (neu.log.log) error logging mechanism
    """
    # RGB color definitions
    BLACK = (0, 0, 0)
    BLUE = (173, 189, 255)
    GRAY = (210, 210, 210)
    GREEN = (206, 247, 181)
    ORANGE = (255, 204, 154)
    PURPLE = (222, 189, 255)
    RED = (255, 140, 148)
    TEAL = (149, 255, 253)
    WHITE = (255, 255, 255)
    YELLOW = (254, 255, 153)

    def __init__(this, log, actorMap, predMap, modMap, objMap):
        """
        Sets up and displays the initial GUI elements.
        
        Raises an Exception if anything went wrong.
        """
        # Init the SDL's main Surface
        pygame.init()
        this.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

        # Assign the logger
        this.log = log

        # Show the mouse cursor
        pygame.mouse.set_visible(True)

        # Store the images
        this.actorMap = actorMap
        this.predMap = predMap
        this.modMap = modMap
        this.objMap = objMap
        this.actorRectMap = {} # tuple/rect -> prefix/word
        this.predRectMap = {} # ...
        this.modRectMap = {}
        this.objRectMap = {}

        # Display some info about our vocabulary
        this.log.log("Info: # actors -- " + str(len(actorMap)))
        this.log.log("Info: # predicates -- " + str(len(predMap)))
        this.log.log("Info: # modifiers -- " + str(len(modMap)))
        this.log.log("Info: # objects -- " + str(len(objMap)))
        this.log.log("Info: Total vocabulary size -- " + \
                str(len(actorMap) + len(predMap) + len(modMap) + len(objMap)))

        # What fonts should we use? (comma-separated list that
        # can be empty)
        this.fonts = ""
    
    def reset(this):
        """
        Reset the entire interface to its default appearance.
        """
        # What's the shape of this monitor?
        screenWidth = this.screen.get_rect().width
        screenHeight = this.screen.get_rect().height
        ratio = screenWidth / screenHeight
        
        # Are we on a "widescreen?" (16:9 or 16:10)
        if ratio >= 1.5:

            # Text message
            this.fontSize = int(math.floor(screenHeight / 8.5))
            this.font = pygame.font.SysFont(this.fonts, this.fontSize)
            textHeight = screenHeight * 0.125
            textMargin = screenHeight * 0.015
            lineWidth = 1

        # We're on a "standard" screen (4:3 or 5:4)
        else:

            # Text message
            this.fontSize = int(math.floor(screenHeight / 10))
            this.font = pygame.font.SysFont(this.fonts, this.fontSize)
            textHeight = screenHeight * 0.1
            textMargin = screenHeight * 0.015
            lineWidth = 1
            
        # Calculate our icon grid measurements
        numRows = 8 # 8 X 13 = 104 icons
        numColumns = 13
        gridHeight = screenHeight - textHeight
        btnHeight = gridHeight / numRows
        btnWidth = screenWidth / numColumns
        actRowWidth = screenWidth - (4 * btnWidth)
        predRowWidth = screenWidth - (2 * btnWidth)

        # White background
        this.screen.fill(GUI.WHITE)

        # Reset the image maps
        this.actorRectMap = {} # tuple/rect -> prefix/word
        this.predRectMap = {} # ...
        this.modRectMap = {}
        this.objRectMap = {}

        # Make an ordered list of icons to lay out
        # in order of top-to-bottom, left-to-right
        actorList = sorted(this.actorMap.iterkeys())
        predList = sorted(this.predMap.iterkeys())
        objList = sorted(this.objMap.iterkeys())
        modList = sorted(this.modMap.iterkeys())
        iconList = actorList + predList + objList + modList

        # Lay out every icon in the grid
        posLeft = 0
        posTop = textHeight
        for iconName in iconList:

            # Which category does this image belong to?
            if iconName in this.actorMap:
                image = this.actorMap[iconName]
                rectMap = this.actorRectMap
                color = GUI.WHITE
            elif iconName in this.predMap:
                image = this.predMap[iconName]
                rectMap = this.predRectMap
                color = GUI.TEAL
            elif iconName in this.objMap:
                image = this.objMap[iconName]
                rectMap = this.objRectMap
                color = GUI.GREEN
            else:
                image = this.modMap[iconName]
                rectMap = this.modRectMap
                color = GUI.RED

            # Track the rect for updates and collisions
            area = (posLeft, posTop, btnWidth, btnHeight)
            iconRatio = 0.7 # Size of icon relative to button
            textRatio = 0.9 # Percentage down from the top that text starts
            iconWidth = btnWidth  * iconRatio
            iconHeight = btnHeight * iconRatio
            iconArea = (posLeft + (btnWidth - iconWidth) / 2, \
                    posTop + (btnHeight - iconHeight) / 2, \
                    iconWidth, iconHeight)
            rectMap[area] = iconName

            # Color the background
            this.screen.fill(color, area)

            # Write the text (centered)
            fontSize = int(math.floor(0.8 * (btnHeight * (1 - iconRatio))))
            font = pygame.font.SysFont(this.fonts, fontSize)
            text = font.render(iconName, True, GUI.BLACK)
            textPos = text.get_rect(centerx = posLeft + btnWidth / 2, \
                    centery = posTop + (btnHeight * textRatio))
            this.screen.blit(text, textPos)

            # Draw the image
            image = pygame.transform.scale(image, (int(iconWidth), int(iconHeight)))
            this.screen.blit(image, iconArea)
            pygame.draw.rect(this.screen, GUI.BLACK, area, 3)
            pygame.display.update(area)

            # Is this the end of the column?
            if posTop + btnHeight >= screenHeight:

                # Move to the right
                posLeft = posLeft + btnWidth
                posTop = textHeight

            else:

                # Just move down
                posTop = posTop + btnHeight

        # Text box
        this.textRect = pygame.Rect(textMargin, \
            textMargin, \
            screenWidth - (2 * textMargin), \
            textHeight - (2 * textMargin))

        # Text box border
        pygame.draw.line(this.screen, GUI.BLACK, \
            (0, textHeight), \
            (screenWidth, textHeight), \
            lineWidth)
            
        # Do we have a previous location that we slid to?
        this.oldPos = None

	    # Refresh the text box
        pygame.display.update()
    
    def setText(this, msg = ""):
        """
        Sets the text message to the given string.

        Input:
            msg -- (string) the text message
        """
        # If the user creates a long sentence, shrink the font
        if len(msg) >= 35 and this.fontShrunk == 0:
            this.fontSize = int(math.floor(0.75 * this.fontSize))
            this.font = pygame.font.SysFont(this.fonts, this.fontSize)
            this.fontShrunk = 1
        elif len(msg) >= 50 and this.fontShrunk == 1:
            this.fontSize = int(math.floor(0.75 * this.fontSize))
            this.font = pygame.font.SysFont(this.fonts, this.fontSize)
            this.fontShrunk = 2
        elif len(msg) >= 70 and this.fontShrunk == 2:
            this.fontSize = int(math.floor(0.75 * this.fontSize))
            this.font = pygame.font.SysFont(this.fonts, this.fontSize)
            this.fontShrunk = 3

        # Render the text
        text = this.font.render(msg, True, GUI.BLACK, GUI.WHITE)
        pygame.draw.rect(this.screen, GUI.WHITE, this.textRect)
        this.screen.blit(text, this.textRect)
        pygame.display.update(this.textRect)

    def slide(this, newPos):
        """
        Draw a slide path to the given position from the last
        position, if we have one.
        """
        # Track rectangles to update
        drawWidth = 8
        updateRects = []

        # Clear the old position
        if newPos == None:
            this.oldPos = None
            return
        
        # Draw a line from our old location to our new one,
        # if applicable
        if this.oldPos != None:
            stepX = newPos[0] - this.oldPos[0]
            stepY = newPos[1] - this.oldPos[1]
            distance = max(abs(stepX), abs(stepY))
            for i in range(distance):
                x = int(this.oldPos[0] + float(i)/distance * stepX)
                y = int(this.oldPos[1] + float(i)/distance * stepY)
                pygame.draw.circle(this.screen, GUI.PURPLE, (x, y), drawWidth)
                updateRects.append(pygame.Rect(x - drawWidth, y - drawWidth, drawWidth * 2, drawWidth * 2))
        
        # Draw the current location and update our position
        pygame.draw.circle(this.screen, GUI.PURPLE, newPos, drawWidth)
        updateRects.append(pygame.Rect(newPos[0] - drawWidth, newPos[1] - drawWidth, drawWidth * 2, drawWidth * 2))
        this.oldPos = newPos
        pygame.display.update(updateRects)

        # Which word was just drawn over?
        role = ""
        word = ""
        for square in updateRects:
            collision = square.collidedict(this.actorRectMap)
            if collision != None:
                role = "actor"
                word = collision[1]
                break
            collision = square.collidedict(this.predRectMap)
            if collision != None:
                role = "pred"
                word = collision[1]
                break
            collision = square.collidedict(this.modRectMap)
            if collision != None:
                role = "mod"
                word = collision[1]
                break
            collision = square.collidedict(this.objRectMap)
            if collision != None:
                role = "obj"
                word = collision[1]
                break
        return (role, word)
