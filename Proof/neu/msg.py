#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Jan 21 11:44:25 EST 2010
# Description: Create and manipulate message text
# $Id: msg.py,v 60ff49b304c1 2012/05/15 20:04:53 wiegand $

# Custom modules
import log

class Message:
    """
    Object that creates and manipulates the
    text of a message (e.g. message construction).
    """
    def __init__(this, log):
        """
        Sets up the initial message text object.
        
        Input:
            log -- (neu.log.log) error logging mechanism

        Raises an Exception if anything went wrong.
        """
        pass

    def make(this, dict):
        """
        Creates the message text based on the given map
        of semantic categories and terms; any category
        with a value of "_blank" will be set to the empty
        string.

        Input:
            dict -- (string->string) the dictionary of
                semantic_category->word mappings, where
                semantic_category includes actor, actorMod,
                part, partMod, object, objectMod, poss, 
                count, or pred; this object will not be
                modified
        """
        # Fix empty categories and capitalize names
        map = dict.copy()
        for category in ["actorMod", "actor", "pred", "partMod", "part", \
                "poss", "count", "objectMod", "object"]:
            if category not in map or map[category] is None or\
                map[category].lower() == "_blank":
                map[category] = ""
            elif map[category] == "i":
                map[category] = "I"

        # Add possessives if we know gender and have a personal object
        maleActors = ["boy", "son", "man"]
        femaleActors = ["girl", "daughter", "woman"]
        personalObjects = ["hair", "hand"]
        poss = map["poss"]
        if map["object"] in personalObjects:
            if map["actor"] in maleActors:
                poss = "his"
            elif map["actor"] in femaleActors:
                poss = "her"

        # Pluralize the objects if we have an appropriate count
        objectMod = map["objectMod"]
        object = map["object"]
        count = map["count"]
        if object != "" and count != "" and count.lower() not in \
            ("1", "more", "less") and \
            not object.endswith("s"):
            object = object + "s"
        elif objectMod != "" and poss == "":
            if objectMod.startswith(("a", "e", "i", "o", "u")):
                objectMod = "an " + objectMod
            else:
                objectMod = "a " + objectMod
        elif object != "" and poss == "" and \
            object not in ("cards", "soccer", "tennis", "dough", "teeth", "batter"):
            if object.startswith(("a", "e", "i", "o", "u")):
                object = "an " + object
            else:
                object = "a " + object
        
        # Fix the participant article, if necessary
        partMod = map["partMod"]
        part = map["part"]
        if part != "" and partMod == "":
            if part.startswith(("a", "e", "i", "o", "u")):
                part = "an " + part
            else:
                part = "a " + part
        
        # Add function words for certain verbs
        # TODO: Do this correctly/statistically!
        verb = map["pred"]
        if verb.startswith(("talk", "sit")) and object != "":
            verb = verb + " on"
        elif verb.startswith(("listen")) and object != "":
            verb = verb + " to"
        elif verb.startswith(("play")) and object != "" and \
            map["object"] not in ("cards", "soccer", "tennis"):
            verb = verb + " with"

        # Piece together the sentence with structural words
        sentList = []
        if partMod != "" or part != "":
            sentList = [map["actorMod"], map["actor"], \
                verb, poss, map["count"], \
                objectMod, object, \
                "with", partMod, part]
        else:
            sentList = [map["actorMod"], map["actor"], \
                verb, poss, map["count"], \
                objectMod, object]
        sent = " ".join(sentList)
        sent = " ".join(sent.split())
        if map["actor"] != "":
            if sent.startswith(("a", "e", "i", "I", "o", "u")):
                sent = "an " + sent
            else:
                sent = "a " + sent
        sent = sent.capitalize() + "."
        return sent
