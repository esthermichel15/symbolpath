Software Dependencies:
    - Python 2.6 -- http://www.python.org/download/
        - On OS X, please upgrade from System Python to Standard Python.
    - Pygame 1.8 -- http://www.pygame.org/download.shtml

To Run the Program:
    - Install Standard Python
    - Install Pygame
    - Edit the settings in [SymbolPath.ini]
    - Open a terminal and run [python SymbolPath.py]
