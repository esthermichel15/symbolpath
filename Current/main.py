#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Fri Jun  1 13:33:13 EDT 2012
# Description: Main program for SymbolPath
# $Id: SymbolPath.py,v 4b4bf8573cea 2012/08/08 19:56:54 wiegand $

from __future__ import division

import kivy
kivy.require("1.3.0")

from kivy.app import App
from kivy.lang import Builder

import itertools
import operator
import os
import platform
import sys

# Custom modules
from neu.cadlab import *
from GUI import *

class SymbolPath(App):
    """
    Main class for an instance of SymbolPath.
    """

    def build(this):
        """
        Initialization of a Kivy App.

        Returns the root Widget.
        """
        this.scriptHeadName = os.path.splitext(__file__)[0]

        # Get our startup configuration
        iniFileName = this.scriptHeadName + ".ini"
        try:
            this.prefs = ini.Ini(iniFileName)
        except Exception, e:
            print(str(e))
            print("Unable to load INI file -- %s" % iniFileName)
            sys.exit(1)

        # Get our logger
        logFileName = this.scriptHeadName + ".log"
        try:
            this.log = log.Log(logFileName, this.prefs)
        except Exception, e:
            print(str(e))
            print("Unable to open log file -- %s" % logFileName)
            sys.exit(1)

        this.log.info("Loading the Kivy language file")
        kivyFileName = this.scriptHeadName + ".kv"
        if "KivyFileName" in this.prefs:
            kivyFileName = this.prefs["KivyFileName"]
        try:
            Builder.load_file(kivyFileName)
        except Exception, e:
            this.log.critical(str(e))
            this.log.critical("Unable to load Kivy file -- %s" % kivyFileName)
            raise
 
        this.log.info("Caching configuration options")
        this.title = this.scriptHeadName
        this.imageDir = "images"
        if "ImageDir" in this.prefs:
            this.imageDir = this.prefs["ImageDir"]
        this.maxUtterances = 10
        if "MaxUtterances" in this.prefs:
            this.maxUtterances = int(this.prefs["MaxUtterances"])
       
        this.log.info("Initializing the GUI")
        try:
            this.gui = GUI(
                    this.log,
                    this.prefs,
                    this.generateUtterances,
                    this.chooseAndSpeakUtterance)
        except Exception, e:
            this.log.critical(str(e))
            this.log.critical("Unable to create GUI")
            raise

        this.log.info("Initializing TTS engine")
        try:
            this.tts = tts.TTS()
        except Exception, e:
            this.log.critical(str(e))
            this.log.critical("Unable to initialize TTS")
            raise

        this.log.info("Parsing the vocabulary file")
        vocabFileName = this.scriptHeadName + ".vocab"
        if "VocabFile" in this.prefs:
            vocabFileName = this.prefs["VocabFile"]
        try:
            this.vocab = vocab.Vocab(vocabFileName)
        except Exception, e:
            this.log.critical(str(e))
            this.log.critical("Unable to parse vocabulary file -- %s" % vocabFileName)
            raise

        this.log.info("Parsing the sem-grams file")
        semStatsFileName = this.scriptHeadName + ".sstats"
        if "SStatsFile" in this.prefs:
            semStatsFileName = this.prefs["SStatsFile"]
        try:
            this.semStats = semstats.SemStats(semStatsFileName)
        except Exception, e:
            this.log.critical(str(e))
            this.log.critical("Unable to parse sem-grams file -- %s" % semStatsFileName)
            raise

        this.log.info("Loading and caching vocabulary images")
        desiredFiles = {}
        for fileName in this.vocab.fileWords.keys():
            desiredFiles[fileName] = True
        for path, dirs, files in os.walk(this.imageDir, True):
            for file in files:
                fullPath = os.path.abspath(os.path.join(path, file))
                prefix = os.path.splitext(file)[0].strip()
                if prefix in desiredFiles:
                    try:
                        this.gui.loadImage(fullPath, this.vocab.fileWords[prefix])
                    except:
                        this.log.error("Unable to load image -- " + prefix)
                    else:
                        del desiredFiles[prefix]

        this.log.info("Checking for missing vocabulary icons")
        for missingIcon in sorted(desiredFiles.keys()):
            this.log.critical("Missing image file for -- " + missingIcon)
        if len(desiredFiles.keys()) > 0:
            raise Exception("Unable to load all required image files")

        this.log.info("Arranging icons in the vocabulary")
        orderedIcons = (this.vocab.roleWords["actor"] +
                this.vocab.roleWords["pred"] +
                this.vocab.roleWords["object"] +
                this.vocab.roleWords["mod"])
        iconColors = []
        for w in this.vocab.roleWords["actor"]:
            iconColors.append((0.66, 0.9, .22, 0.4))
        for w in this.vocab.roleWords["pred"]:
            iconColors.append((0.15, 0.76, 0.93, 0.3))
        for w in this.vocab.roleWords["object"]:
            iconColors.append((0.98, 0.67, 0.27, 0.4))
        for w in this.vocab.roleWords["mod"]:
            iconColors.append((0.59, 0.32, 0.93, 0.3))
        this.gui.setGridIcons(orderedIcons, iconColors)

        # Default to drawing mode
        this.gui.switchDrawing()
        return this.gui

    def getAllRoleCombos(this, words):
        """
        Given a list of words, generate all possible
        combinations of those words by picking AT MOST one
        word from each role.

        Input:
            words -- listof(str) List of words

        Returns word combinations as listof(tupleof(str)).
        """
        # Create trimmed copies of roleWord lists
        actors = [w for w in words if w in this.vocab.roleWords["actor"]]
        preds = [w for w in words if w in this.vocab.roleWords["pred"]]
        objects = [w for w in words if w in this.vocab.roleWords["object"]]
        mods = [w for w in words if w in this.vocab.roleWords["mod"]]

        # What is the target size of this utterance?
        targetLen = 0
        for l in (actors, preds, objects):
            if len(l) > 0:
                targetLen += 1

        # Generate combinations by choosing AT MOST one word from each role
        actors.append("")
        preds.append("")
        objects.append("")
        mods.append("")
        allCombos = []
        for combo in itertools.product(actors, preds, objects, mods):
            combo = list(combo)
            while "" in combo:
                combo.remove("")

            # Filter for utterances of the target length
            if len(combo) == targetLen or len(combo) == targetLen + 1:
                allCombos.append(tuple(sorted(combo)))

        return allCombos

    def generateUtterances(this, iconVotes):
        """
        Given a dict(icon_name -> num_votes), return
        a list of syntactically correct utterances in
        descending order of probability.

        Input:
            iconVotes -- (dict(str -> int)) Mapping of
                words/phrases to number of votes

        Returns list(utterances) as list(str).
        """
        allCombos = this.getAllRoleCombos(iconVotes.keys())
        voteAvg = sum(iconVotes.values()) / len(iconVotes.values())
        
        # Rate every combination
        semCombos = this.semStats.getGramCounts(allCombos)

        # Combine each combination with the physical path votes
        ratedCombos = []
        for combo in semCombos:
            gram = combo[0]
            score = int(combo[1])
            total = 0
            for word in gram:
                total += int(iconVotes[word]) - voteAvg
            score = (total * 1.5) + (score * 1)
            ratedCombos.append((gram, score))
        ratedCombos.sort(key = operator.itemgetter(1), reverse = True)

        # Return the top combinations
        retCombos = []
        for combo in ratedCombos:

            combo = combo[0]

            # Sort the items into roles
            actor = [w for w in combo if w in this.vocab.roleWords["actor"]]
            pred = [w for w in combo if w in this.vocab.roleWords["pred"]]
            mod = [w for w in combo if w in this.vocab.roleWords["mod"]]
            obj = [w for w in combo if w in this.vocab.roleWords["object"]]
            if len(actor) > 0:
                actor = actor[0]
            else:
                actor = ""
            if len(pred) > 0:
                pred = pred[0]
            else:
                pred = ""
            if len(mod) > 0:
                mod = mod[0]
            else:
                mod = ""
            if len(obj) > 0:
                obj = obj[0]
            else:
                obj = ""

            # Skip utterances that cross predicates without picking one
            if actor != "" and pred == "" and (mod != "" or obj != ""):
                continue

            # Pluralize and add articles
            if (obj != "" and mod in ("more", "less") and
                    obj in ("it")):
                obj = "of " + obj
            elif (obj != "" and
                    obj not in ("bathroom", "coffee", "dinner", "food", "it",
                        "love", "lunch", "me", "medicine", "news",
                        "time", "today", "tomorrow", "tv", "up", "water")):
                if mod == "":
                    if obj.startswith(("a", "e", "i", "o", "u")):
                        obj = "an " + obj
                    else:
                        obj = "a " + obj
                elif mod in ("more", "less") and not obj.endswith("s"):
                    obj = obj + "s"
                elif mod not in ("her", "his", "my"):
                    if mod.startswith(("a", "e", "i", "o", "u")):
                        mod = "an " + mod
                    else:
                        mod = "a " + mod
            if (actor in ("brother", "sister", "friend")):
                actor = "my " + actor
            if (pred != "" and actor not in ("i", "they", "we", "you")):
                if pred == "try":
                    pred = "tries"
                elif pred in ("dress", "go", "watch", "wish"):
                    pred = pred + "es"
                elif pred not in ("am", "are", "is"):
                    pred = pred + "s"

            # Add function words for certain verbs
            # TODO: Do this correctly/statistically!
            if pred.startswith(("sit")) and obj != "":
                pred = pred + " on"
            elif (pred.startswith(("go", "listen", "talk")) and obj != ""):
                pred = pred + " to"
            elif (pred.startswith(("play")) and obj != "" and
                    obj not in ("cards", "game", "soccer", "tennis")):
                pred = pred + " with"
            elif (pred.startswith(("wish")) and obj != ""):
                pred = pred + " for"
            elif (pred.startswith(("think")) and obj != ""):
                pred = pred + " about"

            # Piece together the sentence with structural words
            sentList = [actor, pred, mod, obj]
            sent = " ".join(sentList)
            sent = " ".join(sent.split())
            sent = sent.capitalize() + "."
            if sent.strip() == ".":
                continue

            # Add the utterance to our list, if we still need to
            retCombos.append(sent)
            if len(retCombos) >= this.maxUtterances - 1:
                break

        retCombos.append("(None of the above.)")
        return retCombos

    def chooseAndSpeakUtterance(this, utteranceVotes):
        """
        Given a dict(utterance -> num_votes), send the
        "best" utterance to the TTS engine.

        Input:
            utteranceVotes -- (dict(str -> int)) Mapping of
                utterances to number of votes

        Raises an Exception if anything went wrong.
        """
        bestUtt = max(utteranceVotes.iteritems(), key = operator.itemgetter(1))[0]
        if not bestUtt.startswith("("):
            this.tts.speak(bestUtt)

# Create and run an instance of SymbolPath
# if executed directly
if __name__ == "__main__":
    SymbolPath().run()
