# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Wed Feb  8 12:58:55 EST 2012
# Description: SymbolPath GUI functionality
# $Id: GUI.py,v 4028c02617ab 2012/08/07 19:56:57 wiegand $

from __future__ import division
import ast
import math
import random

import kivy
from kivy.core.window import Window
from kivy.graphics import Color, Ellipse, Rectangle, Point
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.widget import Widget

# Custom modules
from neu.cadlab import *

class GUI(Widget):
    """
    GUI for SymbolPath.
    """

    # Hook the Widgets
    widTotalLayout = ObjectProperty(None)
    widGridLayout = ObjectProperty(None)
    widSelectionLayout = ObjectProperty(None)

    def __interpolatePoints(this, startX, startY, endX, endY, stepSize = 5):
        """
        Interpolate intermediate points between the given two
        endpoints at the specified level of granularity.
        
        Input:
            startX -- (float) X-coordinate of the starting endpoint
            startY -- (float) Y-coordinate of the starting endpoint
            endX -- (float) X-coordinate of the other endpoint
            endY -- (float Y-coordinate of the other endpoint
            stepSize -- (int) Size of steps to interpolate

        Returns a list of coordinate pairs as tuples; raises
        an Exception if anything went wrong.
        """
        points = []

        # Calculate interpolation steps
        deltaX = endX - startX
        deltaY = endY - startY
        distance = math.sqrt(deltaX ** 2 + deltaY ** 2)
        numSteps = int(distance / stepSize)

        # Apply steps to create point coordinates
        for stepNum in xrange(1, numSteps):
            stepRatio = stepNum / numSteps
            x = startX + deltaX * stepRatio
            y = startY + deltaY * stepRatio
            points.append((x, y))

        return points

    def __init__(this, log, prefs, generateFunc, speakFunc):
        """
        Initialize the Widget based on the given preferences.

        Input:
            log -- (Log) Error log
            prefs -- (dict) Startup/INI preferences
            generateFunc -- (callback) Function pointer to
                method that takes dict(word/phrase -> num_votes)
                as dict(str -> int) and returns list(utterances)
                as list(str)
            speakFunc -- (callback) Function pointer to
                method that takes dict(utterance -> num_votes) as
                dict(str -> int) and speaks out the "best" utterance

        Raises an Exception if anything went wrong.
        """
        # Call our parent's constructor
        super(GUI, this).__init__()

        # Assign our arguments
        this.log = log
        this.getUtterancesFunc = generateFunc
        this.chooseAndSpeakFunc = speakFunc

        this.log.debug("Caching GUI configuration")
        this.drawWidth = 10
        if "DrawWidth" in prefs:
            this.drawWidth = int(prefs["DrawWidth"])
        this.drawColor = (-1, -1, -1)
        if "DrawColor" in prefs:
            this.drawColor = ast.literal_eval(prefs["DrawColor"])
        this.numGridRows = 10
        if "NumGridRows" in prefs:
            this.numGridRows = int(prefs["NumGridRows"])

        # TODO: Draw the grid icons
        this.images = {}

        # Create the set of collision widgets
        this.checkWidgets = {}

        # Store the current screen touches (multi-touch)
        this.currTouches = [] # list(touch.uid)
        this.touchPoints = [] # list((x, y))

    def on_touch_down(this, touch):
        """
        Kivy's event handler for when a touch (multi-touch supported!)
        is created: draw a circle over the touch and prepare for
        tracking its future position.

        Input:
            touch -- (Kivy MotionEvent) Touch event information

        Raises an Exception if anything goes wrong.
        """
        this.currTouches.append(touch.uid)

        # Assign a unique group ID based on the touch
        touch.ud["group"] = str(touch.uid)

        # Store the touch information for drawing
        this.touchPoints.append((touch.x, touch.y))
        with this.canvas:

            # Generate and store the color
            touch.ud["color"] = Color(
                    random.randint(40, 90)/100,
                    random.randint(40, 90)/100,
                    random.randint(40, 90)/100,
                    mode = "HSV",
                    group = touch.ud["group"]
                    )

            # Draw and track the points
            touch.ud["lines"] = [Point(
                    points = (touch.x, touch.y),
                    pointsize = 10,
                    source = "images/touch.png",
                    group = touch.ud["group"]
                    )]

    def on_touch_move(this, touch):
        """
        Kivy's event handler for when a touch (multi-touch supported!)
        position moves: draw a smooth line for each particular touch
        and check to see if they've wandered into the "done" area
        of the screen.  If so, clear the screen and switch to
        selection of utterances.

        Input:
            touch -- (Kivy MotionEvent) Touch event information

        Raises an Exception if anything goes wrong.
        """
        # Load this touch's points
        points = touch.ud["lines"][-1].points

        # Grab the coordinates of the last point
        lastX, lastY = points[-2], points[-1]

        # Interpolate points to the new coordinates
        points = this.__interpolatePoints(lastX, lastY, touch.x, touch.y)

        # Draw the points
        for point in points:
            while True:
                try:
                    touch.ud["lines"][-1].add_point(*point)
                    this.touchPoints.append(point)
                except GraphicException:

                    # Add a new Point if we hit Kivy's
                    # coordinate limit of 2^15 - 2 
                    with this.canvas:
                        touch.ud["lines"].append(Point(
                            points = (touch.x, touch.y),
                            pointsize = 10,
                            source = "images/touch.png",
                            group = touch.ud["group"]
                            ))
                    continue

                break


    def on_touch_up(this, touch):
        """
        Kivy's event handler for when a touch disappears
        (multi-touch supported!) from the screen: if we have
        no more touches, remove them from the drawing queue,
        clear the screen and switch to selection of utterances.

        Input:
            touch -- (Kivy MotionEvent) Touch event information

        Raises an Exception if anything goes wrong.
        """
        # TODO: Remove this for production!!!
        #Window.screenshot("screenshot%(counter)04d.png")
        # Is this an active touch?
        if touch.uid in this.currTouches:

            this.currTouches.remove(touch.uid)
            this.canvas.remove_group(touch.ud["group"])
            if len(this.currTouches) <= 0:
                if this.state == "DRAWING":

                    # Weight the beginning and ending points more heavily
                    # TODO: Experiment with beginning/ending multipliers!
                    mult = 100
                    lower = 2
                    upper = len(this.touchPoints) - lower

                    # Tally all collided widgets
                    touchedWords = {}
                    for word, icon in this.checkWidgets.items():
                        cursor = 0
                        for point in this.touchPoints:
                            if icon.collide_point(*point):
                                if word in touchedWords:
                                    if cursor < lower or cursor > upper:
                                        touchedWords[word] += mult
                                    else:
                                        touchedWords[word] += 1
                                else:
                                    if cursor < lower or cursor > upper:
                                        touchedWords[word] = mult
                                    else:
                                        touchedWords[word] = 1
                            cursor += 1

                    # Get possible utterances and switch to selection
                    this.switchSelection(this.getUtterancesFunc(touchedWords))

                else:

                    # Weight the beginning and ending points more heavily
                    # TODO: Experiment with beginning/ending multipliers!
                    mult = 100
                    lower = 2
                    upper = len(this.touchPoints) - lower

                    # Tally all collided widgets
                    touchedUtts = {}
                    for utterance in this.utterances:
                        cursor = 0
                        for point in this.touchPoints:
                            if utterance.collide_point(*point):
                                if utterance.text in touchedUtts:
                                    if cursor < lower or cursor > upper:
                                        touchedUtts[utterance.text] += mult
                                    else:
                                        touchedUtts[utterance.text] += 1
                                else:
                                    if cursor < lower or cursor > upper:
                                        touchedUtts[utterance.text] = mult
                                    else:
                                        touchedUtts[utterance.text] = 1
                            cursor += 1

                    # Speak the phrase, then switch to selection
                    this.chooseAndSpeakFunc(touchedUtts)
                    this.switchDrawing()

                if "group" in touch.ud:
                    this.canvas.remove_group(touch.ud["group"])
                this.clearTouchDrawing()

        # It's a redundant touch, so just clear any leftovers
        else:
            if "group" in touch.ud:
                this.canvas.remove_group(touch.ud["group"])
            this.clearTouchDrawing()

    def clearTouchDrawing(this):
        """
        Remove all current touch drawing instructions
        from the drawing queue.

        Raises an Exception if anything goes wrong.
        """
        this.touchPoints = []

    def loadImage(this, fileName, name):
        """
        Load and cache the given image file and associate it
        with the given name.

        Input:
            fileName -- (string) Image file to load and save;
                preferably the full path to avoid relativity issues
            name -- (string) Case-sensitive name of the image

        Raises an Exception if anything goes wrong.
        """
        this.images[name] = Image(source = fileName)

    def setGridIcons(this, names, colors):
        """
        Arrange the given list of named icons in the grid:
        top-to-bottom, left-to-right.

        Input:
            names -- (listof(str)) Case-sensitive names of
                the images; must already be cached in this.images
            colors -- (listof(tupleof(int))) Background color
                codes for each image

        Raises an Exception if anything goes wrong.
        """
        iconW = 1.0 / math.ceil(len(names) / this.numGridRows)
        iconH = 1.0 / this.numGridRows
        posX = 0.0
        posY = 1.0 - iconH
        for name in names:
            icon = BoxLayout(orientation = "vertical", padding = 5)
            icon.size_hint = (iconW, iconH)
            icon.pos_hint = {"x": posX, "y": posY}
            icon.name = name
            icon.color = colors.pop(0)
            icon.bind(pos = this.resized)
            this.widGridLayout.add_widget(icon)
            this.checkWidgets[name] = icon

            # Increment the position
            posY -= iconH
            if posY < 0.0:
                posY = 1.0 - iconH
                posX += iconW

    def resized(this, instance, value):
        """
        Called when a widget gets resized.

        TODO: Fill in arg details!
        """
        instance.canvas.clear()
        instance.canvas.add(Color(*instance.color))
        instance.canvas.add(Rectangle(pos = (instance.x + 1.5, instance.y + 1.5),
            size = (instance.width - 3, instance.height - 3)))
        instance.clear_widgets()
        lbl = Label(text = instance.name, color = [0, 0, 0, 1], halign = "center", valign = "top")
        img = this.images[instance.name]
        img.size_hint = (1, 0.8)
        lbl.size_hint = (1, 0.2)
        lbl.font_size = lbl.font_size * 1.1
        lbl.texture_update()
        instance.add_widget(img)
        instance.add_widget(lbl)

    def switchSelection(this, utterances):
        """
        Switch to selection of utterances.

        Input:
            utterances -- (listof(string)) List of
                potential utterances to choose from

        Raises an Exception if anything goes wrong.
        """
        # Remove the drawing widgets
        this.widTotalLayout.clear_widgets()

        # Add the selection widgets
        this.widTotalLayout.add_widget(this.widSelectionLayout)

        # Add the utterances
        this.utterances = []
        this.widSelectionLayout.clear_widgets()
        for utterance in utterances:
            newWidget = Button(
                    background_color = (1, 1, 1, 0),
                    color = (0, 0, 0, 1),
                    text = utterance
                    )
            this.utterances.append(newWidget)
            this.widSelectionLayout.add_widget(newWidget)
            newWidget.font_size = newWidget.height * 0.33

        # Set our new state
        this.state = "SELECTION"

    def switchDrawing(this):
        """
        Switch to drawing/selection of icons.

        Raises an Exception if anything goes wrong.
        """
        # Remove the selection widgets
        this.widTotalLayout.clear_widgets()

        # Add the drawing widgets
        this.widTotalLayout.add_widget(this.widGridLayout)

        # Set our new state
        this.state = "DRAWING"
