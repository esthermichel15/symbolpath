Software Dependencies:
    - Python 2.6 -- http://www.python.org/download/
        - On OS X, please upgrade from System Python to Standard Python.
    - Kivy 1.3 -- http://kivy.org/#download

To Run the Program:
    - Install Standard Python
    - Install Kivy
    - Edit the settings in [SymbolPath.ini]
    - Open a terminal and run [python SymbolPath.py]
