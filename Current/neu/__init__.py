#!/usr/bin/env python
# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Wed Jan 25 17:43:04 EST 2012
# Description: Optional description of all modules
#   in this package
# $Id: __init__.py,v b8264fd5355f 2012/07/05 19:11:37 wiegand $

# Used for "from neu import *" command
__all__ = [
        "bci", # BCI framework for interaction with ECE team
        "cadlab", # Communication Analysis and Design Laboratory (CadLab)
        ]
