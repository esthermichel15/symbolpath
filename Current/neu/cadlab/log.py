# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Wed Feb  1 16:13:35 EST 2012
# Description: Facade over Python's logging functionality
# $Id: log.py,v deb606978a9a 2012/07/12 17:30:42 wiegand $

import logging

class Log:
    """
    Simplified logger with standard options that can be
    overridden via a dictionary of preferences.

    ATTN: Please keep unit tests relevant!
    """

    # Allowable log levels
    LOG_LEVELS = {
            "CRITICAL" : logging.CRITICAL,
            "ERROR" : logging.ERROR,
            "WARNING" : logging.WARNING,
            "INFO" : logging.INFO,
            "DEBUG" : logging.DEBUG,
            }

    def __init__(this, logFileName, prefs = None):
        """
        Initialize the logging mechanism with some
        standard settings; customized according to
        the given dictionary.

        Input:
            logFileName -- (string) Log file name
            prefs -- (dict) Logging preferences

        Raises an Exception if anything went wrong.
        """
        return
        if prefs == None:
            prefs = {}

        # Customized settings (defaults first, then customizations)
        this.logFileName = logFileName
        this.logLevel = logging.INFO
        if "LogLevel" in prefs:
            if prefs["LogLevel"] in Log.LOG_LEVELS:
                this.logLevel = Log.LOG_LEVELS[prefs["LogLevel"]]
        this.logFormat = "%(asctime)s; %(created)f; %(levelname)s; %(message)s"
        if "LogFormat" in prefs:
            this.logFormat = prefs["LogFormat"]

        # Create the logger
        this.logger = logging.getLogger(__name__)
        this.logger.setLevel(this.logLevel)

        # Create console handler
        ch = logging.StreamHandler()
        ch.setFormatter(logging.Formatter(this.logFormat))
        this.logger.addHandler(ch)

        # Create file handler
        fh = logging.FileHandler(this.logFileName)
        fh.setFormatter(logging.Formatter(this.logFormat))
        this.logger.addHandler(fh)

    def shutdown(this):
        """
        Pass-through function to logging module.
        """
        return
        logging.shutdown()

    def critical(this, msg):
        """
        Pass-through function to logging module.

        Input:
            msg -- (string) Message to log
        """
        return
        this.logger.critical(msg)

    def error(this, msg):
        """
        Pass-through function to logging module.

        Input:
            msg -- (string) Message to log
        """
        return
        this.logger.error(msg)

    def warning(this, msg):
        """
        Pass-through function to logging module.

        Input:
            msg -- (string) Message to log
        """
        return
        this.logger.warning(msg)

    def info(this, msg):
        """
        Pass-through function to logging module.

        Input:
            msg -- (string) Message to log
        """
        return
        this.logger.info(msg)

    def debug(this, msg):
        """
        Pass-through function to logging module.

        Input:
            msg -- (string) Message to log
        """
        return
        this.logger.debug(msg)
