# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Sat Jan 28 10:35:00 EST 2012
# Description: Startup configuration from an INI file
# $Id: ini.py,v 0ee7bb76b481 2012/02/02 18:44:40 wiegand $

import ConfigParser

class Ini(dict):
    """
    Stores the relevant preferences from an INI file and
    makes them accessible from a dict-like object.

    ATTN: Please keep unit tests relevant!
    """

    def __init__(this, iniFileName):
        """
        Read the settings from the given INI file.

        Input:
            iniFileName -- (string) INI file name

        Raises an Exception if anything went wrong.
        """
        dict.__init__(this)

        this.iniFileName = iniFileName

        # Parse the INI file
        this.config = ConfigParser.SafeConfigParser()
        this.config.optionxform = str # Enable case sensitivity
        this.config.readfp(open(iniFileName))

        # Make sure we have two required sections:
        # "Defaults" and "Custom"
        if not this.config.has_section("Defaults"):
            raise Exception("Missing section [Defaults]!")
        elif not this.config.has_section("Custom"):
            raise Exception("Missing section [Custom]!")

        # Put all the default settings into the dictionary
        for (name, value) in this.config.items("Defaults"):
            dict.__setitem__(this, name, value)

        # Custom settings override default settings
        for (name, value) in this.config.items("Custom"):
            dict.__setitem__(this, name, value)

    def __getitem__(this, key):
        """
        Override the dict[] functionality to retrieve the INI file's
        set value for the given option.

        Input:
            key -- (string) Name of the INI option

        Returns a string with the set value or throws an Exception if
        the key wasn't found.
        """
        return dict.__getitem__(this, key)

    def get(this, key, *args):
        """
        Override the dict.get() functionality to retrieve the INI file's
        set value for the given option.

        Input:
            key -- (string) Name of the INI option

        Returns a string with the set value or throws an Exception if
        the key wasn't found.
        """
        return dict.get(this, key, *args)
