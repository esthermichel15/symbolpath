# Created By: Karl Wiegand (wiegand@ccs.neu.edu)
# Date Created: Thu Feb 14 13:47:16 EST 2013
# Description: Support for Text-to-Speech (TTS) engines

# Kivy framework
import kivy
kivy.require('1.5.1')
from kivy.logger import Logger

# Standard libraries
import subprocess

# Platform-specific imports
__platform = kivy.utils.platform()
if __platform == "android":
    from jnius import autoclass
elif __platform == "linux":
    from espeak import espeak

class TTS:
    """
    Object that handles Text-to-Speech (TTS) tasks.
    """
    def __init__(this):
        """
        Initialize the appropriate TTS system.

        Raises an Exception if anything went wrong.
        """
        this.platform = kivy.utils.platform()

        if this.platform == "android":
            
            this.acLocale = autoclass("java.util.Locale")
            this.acPyAct = autoclass("org.renpy.android.PythonActivity")
            this.acTTS = autoclass("android.speech.tts.TextToSpeech")
            this.tts = this.acTTS(this.acPyAct.mActivity, None)

    def speak(this, text):
        """
        Calls the TTS engine to "speak" the given text.

        Input:
            text -- (string) the text that will be spoken.
        """
        Logger.info("TTS: Speaking \"" + text + "\"")
        if this.platform == "android":

            this.tts.stop()
            this.tts.speak(text, this.acTTS.QUEUE_FLUSH, None)

        elif this.platform == "linux":

            # TODO: Modify module's cancel() to call eSpeak's
            # Terminate() -- the current version doesn't seem
            # to cleanly stop the current synthesis
            if not espeak.is_playing():
                espeak.synth(text)

        elif (this.platform == "win" or
                this.platform == "macosx"):

            subprocess.call(["say", text])

        else:

            print "TTS: " + text
